import java.util.List;

import models.Post;
import models.User;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import play.test.Fixtures;
import play.test.UnitTest;

public class BlogTest extends UnitTest
{
  private User bob;
  private Post post1;
  private Post post2;
  
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  } 
  
  @Before
  public void setUp()
  {
    bob = new User ("bob", "jones", "bob@jones.com", "secret");
    post1 = new Post ("First PostTitle", "First PostContent");
    post2 = new Post("Second PostTitle", "Second PostContent");
    bob.save();
    post1.save();
    post2.save();
  }
  
  @After
  public void cleanUp()
  {
    bob.delete();
  }
  
  @Test
  public void testCreatePost()
  {
    bob.posts.add(post1);
    bob.save();
    
    User user = User.findByEmail("bob@jones.com");
    List<Post> posts = user.posts;
    assertEquals(posts.size(), 1);
    
    Post post = posts.get(0);
    assertEquals(post.title, "First PostTitle");
    assertEquals(post.content, "First PostContent");   
  }

  @Test
  public void testCreateMultiplePosts()
  {
    bob.posts.add(post1);
    bob.posts.add(post2);
    bob.save();
    
    User user = User.findByEmail("bob@jones.com");
    List<Post> posts = user.posts;
    assertEquals(2, posts.size());
    
    Post postA = posts.get(0);
    assertEquals(postA.title, "First PostTitle");
    assertEquals(postA.content, "First PostContent");
    
    Post postB = posts.get(1);
    assertEquals(postB.title, "Second PostTitle");
    assertEquals(postB.content, "Second PostContent"); 
  }
  
  @Test
  public void testDeletePost()
  {
    Post post3 = new Post("Third PostTitle", "Third PostContent");
    post3.save();
    bob.posts.add(post3);
    bob.save();
    
    User user = User.findByEmail("bob@jones.com");
    List<Post> posts = user.posts;
    assertEquals(1, posts.size());
    
    Post postA = posts.get(0);
    posts.remove(postA);
    bob.save();    
    assertEquals(0, posts.size());
  }
}