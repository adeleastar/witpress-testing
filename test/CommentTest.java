import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import play.test.Fixtures;
import play.test.UnitTest;
import models.Comment;
import models.Post;
import models.User;


public class CommentTest extends UnitTest
{
  private User bob;
  private Post post1;

  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
  
  @Before
  public void setUp()
  {
    bob = new User("bob", "jones", "bob@jones.com", "secret");
    post1 = new Post("New PostTitle", "New PostContent");
    bob.posts.add(post1);
    post1.save();
    bob.save();
  }
  
/*  @After
  public void cleanUp()
  {
    bob.delete();
  }*/
  
  @Test
  public void testAddComment()
  {    
    User user = User.findByEmail("bob@jones.com");
    
    Comment comment1 = new Comment(user, "New Comment");
    comment1.save();
        
    Post post = user.posts.get(0);
    post.comments.add(comment1);  
    post.save();
    user.save();
    
    User differentUser = User.findByEmail("bob@jones.com");
    assertEquals("New Comment", differentUser.posts.get(0).comments.get(0).content);
  }
  
  @Test
  public void testAddDeleteComment()
  {
    User user = User.findByEmail("bob@jones.com");
    
    Comment comment1 = new Comment(user, "New Comment");
    comment1.save();
        
    Post post = user.posts.get(0);
    post.comments.add(comment1);  
    post.save();
    user.save();
    assertEquals(1, post.comments.size());
    
    Comment comment = post.comments.get(0);
    post.comments.remove(comment);
    post.save();
    user.save();
    assertEquals(0, post.comments.size());
    
    
    
  }

}
