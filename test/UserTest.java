import org.junit.*;

import java.util.*;

import play.test.*;
import models.*;

public class UserTest extends UnitTest
{
  private User bob; 
  
  @BeforeClass
  public static void loadDB()
  {
    Fixtures.deleteAllModels();
  }
  
  @Before
  public void setUp()
  {
    bob = new User("bob", "jones", "bob@jones.com", "secret");
    bob.save();
  }
  
  @After
  public void cleanUp()
  {
    bob.delete();
  }
  
  @Test
  public void testCreateUser()
  {    
    User testUser = User.findByEmail("bob@jones.com");
    assertNotNull (testUser);
  }
  
  @Test
  public void testFindUser()
  {
    User alice = User.findByEmail("alice@jones.com");
    assertNull(alice);
  }
}
